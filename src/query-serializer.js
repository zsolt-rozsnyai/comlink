import QueryString from 'qs'

export default {
  serialize: function (params) {
    return QueryString.stringify(
      this.sanitize(params),
      {
        encode: false,
        arrayFormat: 'brackets'
      }
    )
  },

  deserialize: function (string) {
    return QueryString.parse(string)
  },
  
  sanitize: function (params, prefix) {
    let params2 = {}
    Object.keys(params).forEach(function (x) {
      if (typeof (params[x]) !== 'function' && x !== 'context') {
        params2[x] = params[x]
      }
    })
    return params2
  }
}
