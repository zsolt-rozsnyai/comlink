const serialize = function (hash) {
  if (!hash || hash.length === 0) return ''
  return '_token=' + encodeURIComponent(JSON.stringify(hash))
}

const deserialize = function (cookies) {
  let response = {}
  if (!cookies) return response
  cookies.split(';').forEach(cookie => {
    cookie = decodeURIComponent(cookie.trim()).split('=')
    if (cookie[0] === '_token') {
      let token = JSON.parse(cookie[1])
      if (token && token['access-token']) response = token
    }
  })
  return response
}

export default async function ({ req, res, route, redirect, store, app }) {
  if (!app.$comlink) return 
  let comlink = app.$comlink
  if (process.server && req._parsedUrl.query && req._parsedUrl.query.substring(0, 12) === 'access-token') {
    comlink.token = {}
    req._parsedUrl.query.split('&').forEach(data => {
      comlink.token[data.split('=')[0]] = decodeURIComponent(data.split('=')[1])
    })
    comlink.token['token-type'] = 'Bearer'
  } else if (process.server) {
    comlink.token = deserialize(req.headers.cookie)
  }
  let promise
  try {
    promise = await comlink
      .validateToken()
      .then(response => {
        if (process.server) {
          let week = new Date()
          week.setDate(week.getDate() + 7)
          res.setHeader('Set-Cookie', serialize(comlink.token) +
            '; Path=/; Expires=' + week)
        }
        store.commit('employee/set', response.data.data)
        store.commit('form/clear')
        if (route.fullPath === '/') {
          redirect('/dashboard')
        }
        return response
      })
      .catch(error => {
        throw error
      })
  } catch (e) {
    if (route.fullPath !== '/') {
      redirect('/')
    }
  }
  return promise
}
