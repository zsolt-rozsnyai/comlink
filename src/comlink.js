import axios from 'axios'
import VueCookie from 'vue-cookie'
import Query from './query-serializer.js'

const tokenHeaders = ['access-token', 'token-type', 'client', 'expiry', 'uid']

const initialCookie = function () {
  if (process.client) {
    const _token = VueCookie.get('_token')
    if (!_token) return {}
    return JSON.parse(_token)
  }
  return {}
}

export default {
  basePath: process.env.apiUrl || '',

  userPath: process.env.userModel || 'user',

  shopId: null,

  token: initialCookie(),

  clearToken: function () {
    this.token = {}
    VueCookie.delete('_token')
  },

  setToken: function (headers) {
    if (headers['access-token']) {
      tokenHeaders.forEach((key) => {
        this.token[key] = headers[key]
      })
      if (process.client) {
        VueCookie.set('_token', JSON.stringify(this.token), { path: '/', expires: '7D' })
      }
    }
  },

  getToken: function () {
    let cookie
    if (process.client) cookie = VueCookie.get('_token')
    return cookie ? JSON.parse(cookie) : this.token
  },

  request: function (method, path, data, headers) {
    let options = {
      method: method,
      url: this.basePath + path,
      data: data,
      headers: this.getToken()
    }
    if (this.shopId) {
      Object.assign(options.headers, {'SHOP_ID': this.shopId})
    }
    if (headers) {
      Object.assign(options.headers, headers)
    }
    console.log('Comlink reuqest', options)
    return axios(options)
      .then((response) => {
        console.log('this. is a resolve')
        this.setToken(response.headers)
        return response
      })
      .catch(function (error) {
        console.log('Error: ', error)
        if (process.client && window.$nuxt && window.$nuxt.$toast) {
          if (error.response) {
            if (error.response.data && error.response.data.errors) {
              window.$nuxt.$toast.error(
                window.$nuxt.$t(
                  'form.errors.validation-count',
                  { count: Object.keys(error.response.data.errors).length }
                )
              )
            } else if (error.response.data && error.response.data.text) {
              window.$nuxt.$toast.error(error.response.data.text)
            } else {
              window.$nuxt.$toast.error(error.response.status)
            }
          } else if (error.request) {
            window.$nuxt.$toast.error(
              window.$nuxt.$t('errors.server-unreachable')
            )
          } else {
            window.$nuxt.$toast.error(
              window.$nuxt.$t('errors.not-sent')
            )
          }
        }
        throw error
      })
  },

  login: function (fields) {
    return this.request('post', '/' + this.userPath + '/auth/sign_in', fields)
  },

  logout: function (fields) {
    return this.request('delete', '/' + this.userPath + '/auth/sign_out', fields)
      .then(response => {
        this.clearToken()
        return response
      })
  },

  validateToken: function () {
    return this.request('get', '/' + this.userPath + '/auth/validate_token')
  },

  delete: function (path, data) {
    return this.request('delete', path, data)
  },

  get: function (path, data = {}, headers) {
    const d = new Date()
    data['a'] = d.getTime()
    return this.request('get', path + '?' + Query.serialize(data), headers)
  },

  patch: function (path, data, headers) {
    return this.request('patch', path, data, headers)
  },

  post: function (path, data) {
    return this.request('post', path, data)
  },

  upload: function (path, formData) {
    return this.request('post', path, formData, { 'Content-Type': 'multipart/form-data' })
  },

  scope: function (namespace, model, scopeName, payload) {
    var path = '/' + namespace + '/' + model + '/scope/' + scopeName
    return this.get(path, payload)
  },

  event: function (namespace, model, id, eventName, payload, headers) {
    var path = '/' + namespace + '/' + model + '/' + id + '/event/' + eventName
    return this.request('post', path, payload, headers)
  }
}
