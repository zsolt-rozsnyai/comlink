'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var serialize = function serialize(hash) {
  if (!hash || hash.length === 0) return '';
  return '_token=' + encodeURIComponent(JSON.stringify(hash));
};

var deserialize = function deserialize(cookies) {
  var response = {};
  if (!cookies) return response;
  cookies.split(';').forEach(function (cookie) {
    cookie = decodeURIComponent(cookie.trim()).split('=');
    if (cookie[0] === '_token') {
      var token = JSON.parse(cookie[1]);
      if (token && token['access-token']) response = token;
    }
  });
  return response;
};

exports.default = async function (_ref) {
  var req = _ref.req,
      res = _ref.res,
      route = _ref.route,
      redirect = _ref.redirect,
      store = _ref.store,
      app = _ref.app;

  if (!app.$comlink) return;
  var comlink = app.$comlink;
  if (process.server && req._parsedUrl.query && req._parsedUrl.query.substring(0, 12) === 'access-token') {
    comlink.token = {};
    req._parsedUrl.query.split('&').forEach(function (data) {
      comlink.token[data.split('=')[0]] = decodeURIComponent(data.split('=')[1]);
    });
    comlink.token['token-type'] = 'Bearer';
  } else if (process.server) {
    comlink.token = deserialize(req.headers.cookie);
  }
  var promise = void 0;
  try {
    promise = await comlink.validateToken().then(function (response) {
      if (process.server) {
        var week = new Date();
        week.setDate(week.getDate() + 7);
        res.setHeader('Set-Cookie', serialize(comlink.token) + '; Path=/; Expires=' + week);
      }
      store.commit('employee/set', response.data.data);
      store.commit('form/clear');
      if (route.fullPath === '/') {
        redirect('/dashboard');
      }
      return response;
    }).catch(function (error) {
      throw error;
    });
  } catch (e) {
    if (route.fullPath !== '/') {
      redirect('/');
    }
  }
  return promise;
};