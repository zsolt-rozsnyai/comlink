'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _qs = require('qs');

var _qs2 = _interopRequireDefault(_qs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  serialize: function serialize(params) {
    return _qs2.default.stringify(this.sanitize(params), {
      encode: false,
      arrayFormat: 'brackets'
    });
  },

  deserialize: function deserialize(string) {
    return _qs2.default.parse(string);
  },

  sanitize: function sanitize(params, prefix) {
    var params2 = {};
    Object.keys(params).forEach(function (x) {
      if (typeof params[x] !== 'function' && x !== 'context') {
        params2[x] = params[x];
      }
    });
    return params2;
  }
};