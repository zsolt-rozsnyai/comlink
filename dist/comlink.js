'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _vueCookie = require('vue-cookie');

var _vueCookie2 = _interopRequireDefault(_vueCookie);

var _querySerializer = require('./query-serializer.js');

var _querySerializer2 = _interopRequireDefault(_querySerializer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var tokenHeaders = ['access-token', 'token-type', 'client', 'expiry', 'uid'];

var initialCookie = function initialCookie() {
  if (process.client) {
    var _token = _vueCookie2.default.get('_token');
    if (!_token) return {};
    return JSON.parse(_token);
  }
  return {};
};

exports.default = {
  basePath: process.env.apiUrl || '',

  userPath: process.env.userModel || 'user',

  shopId: null,

  token: initialCookie(),

  clearToken: function clearToken() {
    this.token = {};
    _vueCookie2.default.delete('_token');
  },

  setToken: function setToken(headers) {
    var _this = this;

    if (headers['access-token']) {
      tokenHeaders.forEach(function (key) {
        _this.token[key] = headers[key];
      });
      if (process.client) {
        _vueCookie2.default.set('_token', JSON.stringify(this.token), { path: '/', expires: '7D' });
      }
    }
  },

  getToken: function getToken() {
    var cookie = void 0;
    if (process.client) cookie = _vueCookie2.default.get('_token');
    return cookie ? JSON.parse(cookie) : this.token;
  },

  request: function request(method, path, data, headers) {
    var _this2 = this;

    var options = {
      method: method,
      url: this.basePath + path,
      data: data,
      headers: this.getToken()
    };
    if (this.shopId) {
      Object.assign(options.headers, { 'SHOP_ID': this.shopId });
    }
    if (headers) {
      Object.assign(options.headers, headers);
    }
    console.log('Comlink reuqest', options);
    return (0, _axios2.default)(options).then(function (response) {
      console.log('this. is a resolve');
      _this2.setToken(response.headers);
      return response;
    }).catch(function (error) {
      console.log('Error: ', error);
      if (process.client && window.$nuxt && window.$nuxt.$toast) {
        if (error.response) {
          if (error.response.data && error.response.data.errors) {
            window.$nuxt.$toast.error(window.$nuxt.$t('form.errors.validation-count', { count: Object.keys(error.response.data.errors).length }));
          } else if (error.response.data && error.response.data.text) {
            window.$nuxt.$toast.error(error.response.data.text);
          } else {
            window.$nuxt.$toast.error(error.response.status);
          }
        } else if (error.request) {
          window.$nuxt.$toast.error(window.$nuxt.$t('errors.server-unreachable'));
        } else {
          window.$nuxt.$toast.error(window.$nuxt.$t('errors.not-sent'));
        }
      }
      throw error;
    });
  },

  login: function login(fields) {
    return this.request('post', '/' + this.userPath + '/auth/sign_in', fields);
  },

  logout: function logout(fields) {
    var _this3 = this;

    return this.request('delete', '/' + this.userPath + '/auth/sign_out', fields).then(function (response) {
      _this3.clearToken();
      return response;
    });
  },

  validateToken: function validateToken() {
    return this.request('get', '/' + this.userPath + '/auth/validate_token');
  },

  delete: function _delete(path, data) {
    return this.request('delete', path, data);
  },

  get: function get(path) {
    var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var headers = arguments[2];

    var d = new Date();
    data['a'] = d.getTime();
    return this.request('get', path + '?' + _querySerializer2.default.serialize(data), headers);
  },

  patch: function patch(path, data, headers) {
    return this.request('patch', path, data, headers);
  },

  post: function post(path, data) {
    return this.request('post', path, data);
  },

  upload: function upload(path, formData) {
    return this.request('post', path, formData, { 'Content-Type': 'multipart/form-data' });
  },

  scope: function scope(namespace, model, scopeName, payload) {
    var path = '/' + namespace + '/' + model + '/scope/' + scopeName;
    return this.get(path, payload);
  },

  event: function event(namespace, model, id, eventName, payload, headers) {
    var path = '/' + namespace + '/' + model + '/' + id + '/event/' + eventName;
    return this.request('post', path, payload, headers);
  }
};